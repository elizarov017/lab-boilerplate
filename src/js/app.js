require('../css/app.css');
import Chart from 'chart.js/auto';
import dayjs from 'dayjs';
const _ = require('lodash');


let users = [
    {
      "gender": "Male",
      "title": "Mr",
      "full_name": "Norbert Weishaupt",
      "city": "Rhön-Grabfeld",
      "state": "Mecklenburg-Vorpommern",
      "country": "Germany",
      "postcode": 52640,
      "coordinates": { "latitude": "-42.1817", "longitude": "-152.1685" },
      "timezone": { "offset": "+9:30", "description": "Adelaide, Darwin" },
      "email": "norbert.weishaupt@example.com",
      "b_date": "1956-12-23T19:09:19.602Z",
      "phone": "079-8291509",
      "id": "fgesrg456dsf234c1",
      "favorite": true,
      "course": null,
      "bg_color": "#1f75cb",
      "note": null,
    },
    {
      "gender": "Male",
      "title": "Mr",
      "full_name": "Claude Payne",
      "id": "PPS2340626T",
      "picture_large": "https://randomuser.me/api/portraits/men/40.jpg",
      "picture_thumbnail": "https://randomuser.me/api/portraits/thumb/men/40.jpg",
      "favorite": true,
      "course": null,
      "bg_color": null,
      "note": null,
    },
    {
      "gender": "Male",
      "title": "Mr",
      "full_name": "Aaron Enoksen",
      "city": "Bruflat",
      "state": "Rogaland",
      "country": "Norway",
      "postcode": "6913",
      "coordinates": { "latitude": "51.4281", "longitude": "-160.4653" },
      "timezone": { "offset": "+3:30", "description": "Tehran" },
      "email": "aaron.enoksen@example.com",
      "id": "FN19068929566",
      "favorite": null,
      "course": null,
      "bg_color": "#dface7",
      "note": null,
    },
    {
      "gender": "Female",
      "title": "Mrs",
      "full_name": "Olivia Storm",
      "city": "Kragerø",
      "country": "Norway",
      "postcode": "3127",
      "coordinates": { "latitude": "57.2663", "longitude": "141.0994" },
      "timezone": { "offset": "+1:00", "description": "Brussels, Copenhagen, Madrid, Paris" },
      "b_date": "1946-06-02T09:26:02.733Z",
      "id": "FN02064618043",
      "picture_large": "https://randomuser.me/api/portraits/women/67.jpg",
      "favorite": false,
      "course": "chemistry",
      "bg_color": "#dface7",
      "note": "Old lady with a cats",
    }
];

let showedUsers = []

function idGenerator() {
    let generatedIds = [];
    return function gen() {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for ( var i = 0; i < 10; i++ ) {
            if (i == 3) result += "-"
            else result += characters.charAt(Math.floor(Math.random() * characters.length));
        }

        if (generatedIds.includes(result)) {
            return gen()
        }

        generatedIds = [...generatedIds, result]
        return result;
    };
}

let generateId =  idGenerator()

const seed = generateId()

const data = {
    labels: [
      'Red',
      'Blue',
      'Yellow',
    ],
    datasets: [{
      label: 'My First Dataset',
      data: [200, 50, 100],
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)',
      ],
      hoverOffset: 4,
    }],
  };

  const config = {
    type: 'pie',
    data,
  };


  const myChart = new Chart(
    document.getElementById('myChart'),
    config,
  );


function getExistingNames() {
    return users.map((el) => el.full_name)
}

function removeExistingUsersAndValidate(newUsers) {
    const userNames = getExistingNames();
    return newUsers.filter((el) => {
        return !userNames.includes(el.full_name) && validateUser(el)})
}

function convertToTeachinderFormat(mockUsers) {
    return mockUsers.map(convertUser)
}

function convertUser(user) {
    return {
        "id": user["id"] && user["id"]["name"] && user["id"]["value"]? `${user["id"]["name"]}-${user["id"]["value"]}` : generateId(),
        "favorite": !!user["favorite"],
        "course": user["course"] ? user["course"] : null,
        "bg_color": user["bg_color"] ? user["bg_color"] : null,
        "note": user["note"] ? user["note"] : null,
        "gender": user["gender"] ? capitalizeString(user["gender"]) : null,
        "title": user["name"] ? user["name"]["title"] ? user["name"]["title"] : null : null,
        "full_name": user["full_name"] ? user["full_name"] : `${capitalizeString(user["name"]["first"])} ${capitalizeString(user["name"]["last"])}`,
        "city": user["location"] ? capitalizeString(user["location"]["city"]) : null,
        "state":  user["location"] ? capitalizeString(user["location"]["state"]) : null,
        "country":  user["location"] ? capitalizeString(user["location"]["country"]) : null,
        "postcode":  user["location"] ? user["location"]["postcode"] : null,
        "coordinates": {
            "latitude": user["location"] ? user["location"]["coordinates"]["latitude"] : null,
            "longitude": user["location"] ? user["location"]["coordinates"]["longitude"] : null
        },
        "timezone": {
            "offset": user["location"] ? user["location"]["timezone"]["offset"] : null,
            "description": user["location"] ? user["location"]["timezone"]["description"] : null
        },
        "email": user["email"] ? user["email"] : null,
        "b_date": user["dob"] ? user["dob"]["date"] : null,
        "age": user["dob"] ? user["dob"]["age"] : 0,
        "phone": user["phone"] ? user["phone"] : null,
        "picture_large": user["picture"] ? user["picture"]["large"] : null,
        "picture_thumbnail": user["picture"] ? user["picture"]["thumbnail"] : null
    }
}

function capitalizeString(str) {
    return str? str[0].toUpperCase() + str.slice(1) : str
}

function capitalizeUser(user) {
    var res = user
    res["country"] = res["country"] ? capitalizeString(res["country"]) : null
    res["full_name"] = res["full_name"] ? capitalizeString(res["full_name"]) : null
    res["gender"] = res["gender"] ? capitalizeString(res["gender"]) : null
    res["note"] = res["note"] ? capitalizeString(res["note"]) : null
    res["state"] = res["state"] ? capitalizeString(res["state"]) : null
    res["city"] = res["city"] ? capitalizeString(res["city"]) : null
    return res
}

function convertAndFilterNewUsers(newUsers) {
    return removeExistingUsersAndValidate(convertToTeachinderFormat(newUsers))
}

function importNewUsers(newUsers) {
    users = users.concat(convertAndFilterNewUsers(newUsers))
}

function isStringAndCapital(str) {
    return str == null || (typeof(str) == "string"
        && str[0].toUpperCase() == str[0]
        && str[0].toLowerCase() != str[0])
}

function validateStringFields(user) {
    return isStringAndCapital(user["full_name"])
        && isStringAndCapital(user["gender"])
        && isStringAndCapital(user["note"])
        && isStringAndCapital(user["state"])
        && isStringAndCapital(user["city"])
        && isStringAndCapital(user["country"])
}

// (123) 456-7890
// +(123) 456-7890
// 123-456-7890
// 123.456.7890
// 1234567890
function validatePhoneNumber(phone) {
    let regexp = new RegExp(/^[+]*[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{2}[-\s\.]{0,1}[0-9]{2}$/)
    return phone == null || (phone.match(regexp) || []).length > 0
}

function validateUser(user) {
    if (!validateStringFields(user)) {
        return false
    }

    if (typeof(user.age) != "number" && user.age != null) {
        return false
    }

    if (user.email != null && !user.email.includes("@")) {
        return false
    }

    return true
}

function proccessFilterByAge(element, age) {
    switch (age["by"]) {
        case "<":
            return element["age"] < age["value"]
        case ">":
            return element["age"] > age["value"]
        case "=":
            return element["age"] == age["value"]
    }
}

// {
//     "country": "",
//     "age": {
//         "value": 0,
//         "by": "" // <, >, ==
//     },
//     "photo": false,
//     "gender": "",
//     "favorite": false
// }

function filterWithParams(params) {
    return _.filter(users, (el) => {
        return (params["country"] ?
            el["country"] == params["country"] : true
        ) &&
        (params["age"]["value"] != null ?
            proccessFilterByAge(el, params["age"]) : true
        ) &&
        (params["gender"] ?
            el["gender"] == params["gender"] : true
        ) &&
        (params["favorite"] ?
        el["favorite"] : true
        ) &&
        (params["photo"] ?
        !!el["picture_large"] : true)
    });
};

// {
//     "fieled": "", // full_name, age, age, country
//     "sortBy": "" // asc | desc
// }

function sortUsersBy(array, params) {
    return params["sortBy"] == "asc" ? _.sortBy(array, [params["field"]]) : _.sortBy(array, [params["field"]]).reverse()
}

function findBy(value) {
    return value ? _.filter(users, (user) => {
        return user["full_name"] && user["full_name"].toLowerCase().includes(value.toLowerCase()) ||
        user["note"] && user["note"].toLowerCase().includes(value.toLowerCase())
    }) : users
}




// {
//     "country": "",
//     "age": {
//         "value": 0,
//         "by": "" // <, >, ==
//     },
//     "gender": "",
//     "favorite": false
// }

function getPercentageByFilter(params) {
    return Math.round((filterWithParams(params).length / users.length) * 10000) / 100
}

function updateTeacherList(newList, page = 1) {

    function getInitials(name) {
        return name.split(" ").map((el) => el[0]).join(". ")
    }

    function getDOB(dob) {
        let date = new Date(dob)
        return '<time>'
            + ((date.getDate() > 9) ? date.getDate() : ("0" + date.getDate()))
            + '.'
            + (date.getMonth() > 8 ? date.getMonth() + 1 : ("0" + (date.getMonth() + 1)))
            + '.'
            + date.getFullYear()
            +  '</time>'
    }

    function getAddress(user) {
        if (user["city"] && user["country"]) return "<address>" + user["city"] + ", " + user["country"] + "</address>"
        if (user["city"]) return "<address>" + user["city"] + "</address>"
        if (user["country"]) return "<address>" + user["country"] + "</address>"
        return ""
    }

    showedUsers = newList

    let teachersListElement = document.getElementById("teachers-list")
    let favoritesListElement = document.getElementsByClassName("favorite-list")[0]

    teachersListElement.innerHTML = ""
    favoritesListElement.innerHTML = ""
    // favoritesListElement.scroll({
    //     left: 0,
    //     top: 0,
    //     behavior: 'smooth'
    // });

    showedUsers.forEach( el => {
        let node = document.createElement('div');
        node.classList = `card ${el["favorite"] ? "favorite " + el["id"] : el["id"]}`
        node.innerHTML = `
        <div class="photo">${el["picture_large"] ? '<img src="' + el["picture_large"] + '" alt="">' : `<span>${getInitials(el["full_name"])}</span>`}</div>
        <div class="info">
            <p>${el["full_name"]}</p>
            <span>${el["country"] ? el["country"] : el["state"] ? el["state"] : el["city"] ? el["city"] : ""}</span>
        </div>`


        node.onclick = () => {
            let popup = document.createElement("div")
            let user = el
            let dob = dayjs(user["b_date"])
            let daysToBD;
            if (dob) {
                dob = dob.year(dayjs().year())
                if (dob.diff(dayjs(), 'day') < 0) dob = dob.year(dayjs().year() + 1)

                daysToBD = dob.diff(dayjs(), 'day')
            }
            popup.classList = "big-popup"
            popup.innerHTML =
            `
            <div class="popup-bg"></div>
            <div class="big-card">
                <span class="${user["favorite"] ? "star favorite-big" : "star not-favorite-big"}"><img class="" src=""></img></span>
                <h1>Teacher information</h1>
                <section class="main-info">
                <div class="big-photo"><img src="${user["picture_large"] ? user["picture_large"] : '../images/no-photo.jpg'}" alt=""></div>
                <div class="text-info">
                    <span class="close">&times;</span>
                    <br>
                    <h2>${user["full_name"]}</h2>
                    ${user["gender"] ? '<p><span>' + user["gender"] + '</span></p>' : ""}
                    <br>
                    ${user["age"] ? '<p>' + user["age"] + " years old / " + getDOB(user["b_date"]) + '</p>' : ""}
                    ${daysToBD ? '<p style="font-size: 1rem">' + daysToBD + " day(s) to the Birthday" + '</p>' : ""}
                    <br>
                    ${user["email"] ? '<p><a href="mailTo:' + user["email"] + '">' + user["email"] + '</a></p>' : ""}
                    ${user["phone"] ? '<p><a href="tel:' + user["phone"] + '">' + user["phone"] + '</a></p>' : ""}
                    <br>
                    ${getAddress(user)}
                    <br>
                </div>
                </section>
                <section class="additional-info">
                <br>
                ${user["note"] ? "<p>" + user["note"] + "</p>" : ""}
                <br>
                ${user["coordinates"] ?
                `<details class="map-toggle">
                    <summary>Toggle map</summary>
                    <br>
                    <div id="map"></div>
                </details>`
                : ""}
                </section>
            </div>
            `

            if (user["coordinates"]) {
                let map;
                popup.getElementsByClassName("map-toggle")[0].onclick = () => {
                    if (map) return;
                    // "-42.1817", "longitude": "-152.1685"
                    map = L.map('map').setView([user.coordinates.latitude, user.coordinates.longitude], 5);

                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                        accessToken: "pk.eyJ1IjoiZWxpemFyb3YiLCJhIjoiY2t3Y2RjMGdhMWgwcjJ4cDJ2bXljbmhlbCJ9.9ZJNAQVAq7GaarkK-j4RQg",
                        maxZoom: 18,
                        id: 'mapbox/streets-v11',
                        tileSize: 512,
                        zoomOffset: -1,
                    }).addTo(map);

                    L.marker([user.coordinates.latitude, user.coordinates.longitude])
                    .addTo(map)
                }
            }

            popup.getElementsByClassName("star")[0].onclick = () => {
                user["favorite"] = !user["favorite"]
                popup.getElementsByClassName("star")[0].classList = user["favorite"] ? "star favorite-big" : "star not-favorite-big"
            }


            popup.getElementsByClassName("close")[0].onclick = () => {
                popup.remove();
                updateTeacherList(showedUsers)
            }

            popup.getElementsByClassName("popup-bg")[0].onclick = () => {
                popup.remove();
                updateTeacherList(showedUsers)
            }

            document.getElementById("teachers-list").appendChild(popup)
        }

        if (el["favorite"]) {
            let favoriteNode = node.cloneNode("deep")
            favoriteNode.onclick = node.onclick
            favoritesListElement.appendChild(favoriteNode)
        }

        updateStatistics(showedUsers, "age")

        teachersListElement.appendChild(node)
    })

    document.getElementById("teachers-section").getElementsByClassName("active-page")[0].innerHTML = page

    document.getElementById("teachers-section").getElementsByClassName("n-page")[0].onclick = () => {
        if (page == 5) return
        updateTeacherList(convertAndFilterNewUsers(getUsers({"seed": seed, "page": page}, 10)), ++page)
    }


    document.getElementById("teachers-section").getElementsByClassName("p-page")[0].onclick = () => {
        if (page == 1) return
        updateTeacherList(convertAndFilterNewUsers(getUsers({"seed": seed, "page": page}, 10)), --page)
    }


}

function getQueryParams() {
    let filter = document.getElementsByClassName("filter")[0]
    return {
        "country": capitalizeString(filter.getElementsByClassName("country-input")[0].getElementsByTagName("input")[0].value) || null,
        "age": {
            "value": Number(filter.getElementsByClassName("age-selector")[0].getElementsByTagName("input")[0].value) || null,
            "by": filter.getElementsByClassName("age-selector")[0].getElementsByTagName("select")[0].value // <, >, ==
        },
        "photo": filter.getElementsByClassName("with-photo")[0].getElementsByTagName("input")[0].checked,
        "gender": filter.getElementsByClassName("gender-selector")[0].getElementsByTagName("select")[0].value || null,
        "favorite": filter.getElementsByClassName("only-favorites")[0].getElementsByTagName("input")[0].checked
    }
}

function updateStatistics(users, value) {
    let allUsers = [...users]
    let currData = {};

    allUsers.forEach(el => {
        if (!el[value]) {
            currData["Not specified"] ? currData["Not specified"] += 1 : currData["Not specified"] = 1
            return
        }

        if (value == "age") {
            let roundedAge = Math.floor(parseInt(el[value]) / 10) * 10
            currData[`${roundedAge}-${roundedAge + 9}`] ? currData[`${roundedAge}-${roundedAge + 9}`] += 1 : currData[`${roundedAge}-${roundedAge + 9}`] = 1
            return
        }
        currData[el[value]] ? currData[el[value]] += 1 : currData[el[value]] = 1
    })


    myChart.data.labels = (Object.keys(currData));
    myChart.data.datasets[0].backgroundColor = [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)',
        'rgb(0, 255, 166)',
        'rgb(144, 33, 255)',
        'rgb(144, 255, 33)',
        'rgb(255, 148, 33)'
      ]
    myChart.data.datasets[0].data = Object.values(currData);
    myChart.update();

}


function search() {
    updateTeacherList(findBy(document.getElementsByClassName("search-input")[0].value))
    document.getElementsByClassName("search-input")[0].value = ""
}

function addTeacherForm() {
    let popup = document.createElement("div")
    popup.classList = "big-popup"
    popup.id = "popup-add-teacher"

    popup.innerHTML = `
        <div class="popup-bg"></div>
        <div class="big-card add-form-teacher" data-attribute="#4dsaf4">
            <h1>Add Teacher</h1>
            <span class="close">&times;</span>
            <main>
               <form class="add-teacher-form">
                   <ul class="form-list">
                       <li class="name field">
                           <label for="fname">Full name:</label>
                           <input type="text" id="fname" data-attr="full_name" autocomplete="name" required>
                       </li>
                       <li class="sex field">
                           <label for="sgr">Sex:</label>
                           <input type="radio" name="sex" id="fsexf" data-attr="gender" value="female" required>
                           <label for="fsexf">Female</label>
                           <input type="radio" name="sex" id="fsexm" data-attr="gender" value="male" required>
                           <label for="fsexm">Male</label>
                       </li>
                       <li>
                           <label for="bday">Date of birth:</label>
                           <input type="date" name="bday" id="bday" data-attr="b_date" autocomplete="bday-day" required>
                       </li>
                       <li>
                           <label for="femail">Email:</label>
                           <input type="email" name="email" id="form-email" data-attr="email" placeholder="your-email@example.com" autocomplete="email" required>
                       </li>
                       <li>
                           <label for="phnum">Phone number:</label>
                           <input type="tel" name="tel" id="phnum" data-attr="phone" placeholder="0324532187" pattern="^[+]*[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{2}[-\s\.]{0,1}[0-9]{2}$" required>
                       </li>
                       <li>
                           <label for="bcolor">Background color:</label>
                           <input type="color" data-attr="bg_color" name="color" id="bcolor">
                       </li>
                       <li>
                           <label for="country">Country: </label>
                           <input type="text" name="country" data-attr="country" id="country" autocomplete="country">
                       </li>
                       <li>
                           <label for="city">City: </label>
                           <input type="text" name="city" data-attr="city" id="city" autocomplete="city">
                       </li>
                       <li>
                           <label for="msg">Message: </label>
                           <textarea name="msg" data-attr="note" cols="20" rows="3"></textarea>
                       </li>
                       <li>
                           <input class="btn" type="submit" value="Send">
                       </li>
                   </ul>
               </form>
           </main>
        </div>
        `
        popup.getElementsByClassName("close")[0].onclick = () => {
            popup.remove();
            updateTeacherList(users)
        }

        popup.getElementsByClassName("popup-bg")[0].onclick = () => {
            popup.remove();
            updateTeacherList(users)
        }

        popup.getElementsByTagName("form")[0].onsubmit = () => {
            // let formData = new FormData(popup.getElementsByTagName("form")[0])
            let user = {
                "id": generateId(),
                "favorite": false,
                "course": null,
                "bg_color": null,
                "note": null,
                "gender": null,
                "title": null,
                "full_name": "",
                "city": null,
                "state":  null,
                "country":  null,
                "postcode":  null,
                "coordinates": null,
                "timezone": {
                    "offset": null,
                    "description": null
                },
                "email": null,
                "b_date": null,
                "age": 0,
                "phone": null,
                "picture_large": null,
                "picture_thumbnail": null
            }

            let currAttr = ""
            Array.from(popup.getElementsByTagName("input")).forEach(el => {
                currAttr = el.getAttribute("data-attr")
                if (currAttr == "gender" && el.id == "fsexf") {
                    user["gender"] = el.checked ? "Female" : "Male"
                    return
                }
                else if (currAttr == "gender" && el.id == "fsexm") return
                else if (!currAttr) return

                user[el.getAttribute("data-attr")] = el.value
            })

            user["note"] = popup.getElementsByTagName("textarea")[0].value
            user = capitalizeUser(user)
            user["title"] = user["gender"] == "Female" ? "Mrs" : user["gender"] == "Male" ? "Ms" : ""
            users = [...users, user]
            postUser(user)
            popup.remove()
            alert("Successfuly added teacher!")
            updateTeacherList(users)
            updateStatistics(users, "age")
            return false
        }

    document.getElementsByTagName("body")[0].appendChild(popup)
}

function formUrl(url, params, results) {
    let query = "?results=" + results;
    for (let key of Object.keys(params)) {
        if (params[key]) query += "&" + key + "=" + params[key];
    }
    return url + query;
}

function getUsers(params, results) {
    let xmlHttp = new XMLHttpRequest();
    let baseUrl = "https://randomuser.me/api/"

    xmlHttp.open( "GET", formUrl(baseUrl, params, results), false );
    xmlHttp.send( null );
    return JSON.parse(xmlHttp.responseText)["results"];
}

function postUser(user) {
    let xmlHttp = new XMLHttpRequest();
    let baseUrl = "http://localhost:3000/users"

    xmlHttp.open( "POST", baseUrl, false );
    xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xmlHttp.send(JSON.stringify(user));
    return JSON.parse(xmlHttp.responseText);
}

function searchPopup() {
    let popup = document.createElement("div")
    const nat = {
        "Australia": "au",
        "Brazil": "br",
        "Canada": "ca",
        "Switzerland": "ch",
        "Germany": "de",
        "Denmark": "dk",
        "Spain": "es",
        "Finland": "fi",
        "France": "fr",
        "United Kingdom": "gb",
        "Ireland": "ie",
        "Iran": "ir",
        "Norway": "no",
        "Netherlands": "nl",
        "New Zealand": "nz",
        "Turkey": "tr",
        "United States": "us"
    }
    popup.classList = "big-popup"

    popup.innerHTML = `
    <div class="popup-bg"></div>
         <div class="big-card search-teacher">
            <h1>Search Menu</h1>
            <span class="close">&times;</span>
            <main>
               <div class="search-by-text">
                  <div class="header-menu header-buttons">
                     <input class="search-input" type="text" placeholder="Enter text to search">
                     <button id="search-btn-text">Search</button>
                  </div>
               </div>
               <div class="separator">
                  <hr>
               </div>
               <div class="filter search-by-params">
                  <span class="gender-selector"><label for="select-gender"> Gender: </label>
                     <select type="select" name="gender" id="select-gender-search">
                        <option value="">-</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                  </select></span>
                  <span class="age-selector"><label for="select-age""> Age: </label>
                     <input type="number" name="age" id="inp-age-search">
                  </span>
                  <span class="country-input"><label for="input-country"> Country: </label>
                     <input type="text" name="age" id="input-country-search">
                  </span>
                  <div class="header-buttons search-params">
                     <button id="search-btn-params">Search</button>
                  </div>
               </div>
           </main>
        </div>`

    popup.getElementsByClassName("close")[0].onclick = () => {
        popup.remove();
        updateTeacherList(users)
    }

    popup.getElementsByClassName("popup-bg")[0].onclick = () => {
        popup.remove();
        updateTeacherList(users)
    }

    popup.querySelector('#search-btn-text').onclick = () => {
        search();
        popup.remove();
    };

    popup.querySelector('#search-btn-params').onclick = () => {
        let country = nat[capitalizeString(popup.querySelector("#input-country-search").value)]
        if (!country && popup.querySelector("#input-country-search").value) {
            alert(`No such country available\n\nYou can use these countries:\n
Australia, Brazil, Canada, Switzerland, Germany, Denmark, Spain, Finland, France, United Kingdom, Ireland, Iran, Norway, Netherlands, New Zealand, Turkey, United States`)
            return
        }
        else {
            let params = {
                "nat": country || null,
                "gender": popup.querySelector("#select-gender-search").value || null,
            }
            let users = getUsers(params, 50)
            if (popup.querySelector("#inp-age-search").value) users = users.filter((el) => el["dob"]["age"] == popup.querySelector("#inp-age-search").value)
            updateTeacherList(convertAndFilterNewUsers(users))
            popup.remove();
        }
    };

    document.getElementsByTagName("body")[0].appendChild(popup)
}

Array.from(document.getElementsByClassName("add-teacher-btn")).forEach(el => {
    el.onclick = () => {
        addTeacherForm()
    }
})


document.getElementById('search-btn').onclick = () => {
    searchPopup()
};


document.getElementsByClassName('next')[0].onclick = () => {
    document.getElementsByClassName("favorite-list")[0].scrollBy(1370, 0)
};

document.getElementsByClassName('prev')[0].onclick = () => {
    document.getElementsByClassName("favorite-list")[0].scrollBy(-1370, 0)
};

document.getElementsByClassName("btn-filter")[0].onclick = () => {
    updateTeacherList(filterWithParams(getQueryParams()))
}

importNewUsers(getUsers({}, 50))

updateTeacherList(users)

updateStatistics(users, "age")

document.getElementById('stat-buttons')
    .childNodes
    .forEach((node) => {
      node.addEventListener('click', () => {
        updateStatistics(users, (node.getAttribute('value')));
      });
    });


